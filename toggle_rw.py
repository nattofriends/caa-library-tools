#!/usr/bin/python3
# Rarely used. Before btsync was actually installed,
# I thought it would be a good idea to run the library volumes
# as RO most of the time for safety.
# This script provides an easy way to toggle the rwness of
# the library volumes.

# This doesn't need to be run inside the virtualenv.

from subprocess import check_output as _call

import itertools
import os
import re
import sys

call = lambda *args, **kwargs: _call(*args, universal_newlines=True, shell=True, **kwargs)

print("CAA library ro/rw toggle tool")

if os.geteuid() != 0:
    sys.exit("This script needs to be run as root.")

# Library A, B, C
# These are NTFS serial numbers, I think
LIBRARY_DISKS = {
    "a": "F2401FD0401F9A85",
    "b": "6C72DED372DEA15E",
    "c": "9868819068816E36",
    "d": "00E63CD2E63CCA24",
}

MOUNT_POINT = "/media/caa-library"
MOUNT_OPTS = "{rwness},users,uid=tzhu,gid=tzhu,dmask=022,fmask=133,inherit".format
RWNESS = {"ro", "rw"}

blkid = call("blkid").split("\n")
blkid = itertools.product(blkid, LIBRARY_DISKS.values())
blkid = filter(lambda tup: tup[1] in tup[0], blkid)
blkid = map(lambda tup: (re.search(r"^([^:]+):", tup[0]).group(1), tup[1]), blkid)
blkid = dict(blkid)

mount = call("mount").split("\n")
mount = itertools.product(mount, blkid.keys())
mount = filter(lambda tup: tup[1] in tup[0], mount)
mount_rwness = map(lambda tup: (tup[1], re.search(r'\(([^\)]+)\)', tup[0]).group(1)), mount)
mount_rwness = map(lambda tup: list(filter(lambda mount_arg: mount_arg in RWNESS, tup[1].split(',')))[0], mount_rwness)
mount_rwness = set(mount_rwness)

if len(mount_rwness) != 1:
    sys.exit('Won\'t toggle rwness, currently inconsistent')

target_rwness = (RWNESS - mount_rwness).pop()
print('Current rwness is {0}'.format(mount_rwness.pop()))

print('Stopping samba...')
call('service samba stop')

for mount_folder, uuid in LIBRARY_DISKS.items():
    print('Remounting {mount_folder}...'.format(mount_folder=mount_folder))

    mount_point = os.path.join(MOUNT_POINT, mount_folder)
    call('umount {mount_point}'.format(mount_point=mount_point))
    call('mount -o {opts} -U {uuid} {mount_point}'.format(
        opts=MOUNT_OPTS(rwness=target_rwness),
        uuid=uuid,
        mount_point=mount_point
    ))

print('Remounted as {0}'.format(target_rwness))

print('Starting samba...')
call('service samba start')
