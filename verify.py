#!/usr/bin/python3
from collections import defaultdict
from collections import namedtuple
from operator import attrgetter
from subprocess import call
from subprocess import check_output
from tempfile import NamedTemporaryFile
import logging
import os
import re


EXCLUDE = ['.git', '$RECYCLE.BIN', 'manage', 'System Volume Information']


log = logging.getLogger(__name__)


class SHA1Entry(namedtuple('SHA1Entry', 'hash filename')):

    def __str__(self):
        return "{} {}".format(self.hash, self.filename)


def load_sha1sums(filename):
    sums = open(filename).readlines()
    return [SHA1Entry(*line.strip().split(None, 1)) for line in sums]


def iterate_directory(path):
    for root, _, files in os.walk(path):
        if any(exclude_path in root for exclude_path in EXCLUDE):
            continue

        for filename in files:
            yield (root, filename)


def move_sha1sums():
    log.info('Moving sha1sums of moved files')

    sums = load_sha1sums('SHA1SUMS')

    basename_to_sums_map = defaultdict(list)

    for entry in sums:
        basename_to_sums_map[os.path.basename(entry.filename)].append(entry)

    for root, filename in iterate_directory('..'):
        full_path = os.path.join(root, filename)
        entries = basename_to_sums_map[filename]

        for entry in entries:
            # A candidate for replacement?
            if not os.path.exists(entry.filename):
                basename_to_sums_map[filename].remove(entry)
                basename_to_sums_map[filename].append(SHA1Entry(entry.hash, full_path))

    sums = [entry for basename_sums in basename_to_sums_map.values() for entry in basename_sums]

    with open('SHA1SUMS', 'w') as sums_file:
        for entry in sorted(sums, key=attrgetter('filename')):
            sums_file.write(str(entry) + '\n')


def add_sha1sums():
    log.info('Adding new sha1sums')

    sums = load_sha1sums('SHA1SUMS')

    filenames = {entry.filename for entry in sums}

    for root, filename in iterate_directory('..'):
        full_path = os.path.join(root, filename)
        if full_path not in filenames:
            print("Found {}".format(full_path))
            output = str(check_output(
                ('sha1sum', full_path)
            ), encoding='utf8').strip()
            sums.append(SHA1Entry(*output.split(None, 1)))

    with open('SHA1SUMS', 'w') as sums_file:
        for entry in sorted(sums, key=attrgetter('filename')):
            sums_file.write(str(entry) + '\n')


def check_crc32():
    log.info('Checking crc32')

    with NamedTemporaryFile() as temp_sfv:
        for root, filename in iterate_directory('..'):
            m = re.search(
                r'.*[^A-Fa-f0-9]([A-Fa-f0-9]{8})[^A-Fa-f0-9].*(avi|mp4|mkv|rm|rmvb|wmv|ogm|mpg|divx|asf)',
                filename
            )
            temp_sfv.write(bytes(
                "{} {}\n".format(os.path.join(root, filename), m.group(1)),
                encoding='utf-8',
            ))
            temp_sfv.flush()

        call(['cksfv', '-f', temp_sfv.name, '-q'])


def check_sha1():
    log.info('Checking sha1')
    call(['sha1sum', '-c', 'SHA1SUMS', '--quiet'])


def main():
    logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s', level=0)

    check_crc32()
    move_sha1sums()
    check_sha1()
    add_sha1sums()


if __name__ == '__main__':
    main()
